import numpy as np


#Load data for selected variables
def load_data(EC, PhIndex, NHTor, CF, NHcone , deg, int_comp) -> dict :
    #Create dictionaries to store the requested data
    variable_dict: dict = {}
    full_dict: dict = {}
    full_dict_int: dict = {}

    #Loop through the requested degrees to load only the data needed
    for i in deg:
        name10 = f"spec_{str(i).zfill(2)}_03_10"
        values10 = np.loadtxt(f"par_files/dir_par_{EC}_{PhIndex}_To_{NHTor}_{CF}_Co_{NHcone}/deg{str(i-5).zfill(2)}_{str(i+4).zfill(2) if i != 85 else str(i+5).zfill(2)}/d{str(i).zfill(2)}_03_10.txt")
        name200 = f"spec_{str(i).zfill(2)}_10_200"
        values200 = np.loadtxt(f"par_files/dir_par_{EC}_{PhIndex}_To_{NHTor}_{CF}_Co_{NHcone}/deg{str(i-5).zfill(2)}_{str(i+4).zfill(2) if i != 85 else str(i+5).zfill(2)}/d{str(i).zfill(2)}_10_200.txt")
        variable_dict[name10]=values10
        variable_dict[name200]=values200
        full_name = f"full_spec_{str(i).zfill(2)}"
        full_values = np.concatenate((variable_dict[name10], variable_dict[name200]), axis=0)
        full_values[:,2] /= (np.cos(np.deg2rad(i-5)) - np.cos(np.deg2rad(i+5)))
        full_dict[full_name]=full_values

    #get int components if asked
    if int_comp:
        variable_dict_int: dict = {}

        #Loop through the requested degrees to load only the data needed
        for i in deg:
            name10_int = f"spec_{str(i).zfill(2)}_03_10"
            values10_int = np.loadtxt(f"par_files/dir_par_{EC}_{PhIndex}_To_{NHTor}_{CF}_Co_{NHcone}/deg{str(i-5).zfill(2)}_{str(i+4).zfill(2) if i != 85 else str(i+5).zfill(2)}/d{str(i).zfill(2)}_03_10_INT.txt")
            name200_int = f"spec_{str(i).zfill(2)}_10_200"
            values200_int = np.loadtxt(f"par_files/dir_par_{EC}_{PhIndex}_To_{NHTor}_{CF}_Co_{NHcone}/deg{str(i-5).zfill(2)}_{str(i+4).zfill(2) if i != 85 else str(i+5).zfill(2)}/d{str(i).zfill(2)}_10_200_INT.txt")
            variable_dict_int[name10_int]=values10_int
            variable_dict_int[name200_int]=values200_int
            full_name_int = f"full_spec_{str(i).zfill(2)}"
            full_values_int = np.concatenate((variable_dict_int[name10_int], variable_dict_int[name200_int]), axis=0)
            full_values_int[:,2] /= (np.cos(np.deg2rad(i-5)) - np.cos(np.deg2rad(i+5)))
            full_dict_int[full_name_int]=full_values_int       

    return full_dict, full_dict_int