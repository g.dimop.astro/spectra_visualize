# app.py
import streamlit as st
import pandas as pd
import copy
from make_plot import create_plot, save_figure, plot_all, save_aio
from load import load_data
from params import params

st.set_page_config(layout='wide')
st.title("RefleX simulations preview")


# Initialize the session state for the plot list
if 'plot_list' not in st.session_state:
    st.session_state['plot_list'] = []

# Initialize the session state for the figure
if 'figure_list' not in st.session_state:
    st.session_state['figure_list'] = []

# Initialize the session state for the aio figure
if 'aio_fig' not in st.session_state:
    st.session_state['aio_fig'] = None


#Function to help identify what object from the plot_list to delete
clicked_button_key = None
def set_clicked_button_key(key):
    global clicked_button_key
    clicked_button_key = key


def main():
    st.sidebar.title('Parameter Selection')
    selected_option_EC, selected_option_PhIndex, selected_option_NHTor, selected_option_CF, selected_option_NHcone, degrees, checkbox_value = params()
    num=0
    
    col1, col2 = st.sidebar.columns([0.5,0.5])
    with col1:
        add = st.sidebar.button('Add model +', help='Add selected set of variables')
    with col2:
        aio_plot = st.sidebar.button('Plot all models', help= 'Create a single plot to include the data from all variable sets')
    
    col3, col4 = st.sidebar.columns(2)
    with col3:
        plot=st.sidebar.button('Plot separately', help='Crate a plot for each set of variables individually')
    with col4:
        save_all = st.sidebar.button('Save all Figures', help='Save all created individual plots')


    clear = st.sidebar.button('Clear all', help='Erease all selected sets of variables and created plots')

    

    if add:
        st.session_state['plot_list'].append([
            selected_option_EC,
            selected_option_PhIndex,
            selected_option_NHTor,
            selected_option_CF,
            selected_option_NHcone,
            list(degrees),
            checkbox_value
        ])
        st.session_state['figure_list'].append(None)
        st.session_state['aio_fig'] = None

    if clear:
        st.session_state['plot_list'] = []
        st.session_state['figure_list'] = []
        st.session_state['aio_fig'] = None

    st.header('Selected Plots')
    if len(st.session_state['plot_list']) == 0:
        st.subheader('Please select set of variables')
    for i in st.session_state['plot_list']:

        st.subheader(f'Plot Nr. {num+1}')

        col4, col5 = st.columns([3,1])

        with col4:
            data_plot={
                'Variables': [ 'EC:', 'Photon Index:', 'NH Torus:', 'Covering Factor:', 'NH Cone:', 'Angles:', 'Include INT components:'],
                'Values': [str(val) for val in [i[0], i[1], i[2], i[3], i[4], i[5], i[6]]]
            }
            st.table(pd.DataFrame(data_plot))
        with col5:
            spec_plot_key = f'spec_plot_{num}'
            spec_plot = st.button('Plot', key=spec_plot_key, on_click=(lambda key: lambda: set_clicked_button_key(key))(spec_plot_key))
            save_key=f'save_{num}'
            save = st.button('Save Plot', key=save_key, on_click=(lambda key: lambda: set_clicked_button_key(key))(save_key))
            delete_key = f'delete_{num}'
            delete = st.button('Delete Plot', key=delete_key, on_click=(lambda key: lambda: set_clicked_button_key(key))(delete_key))
            # edit_key = f'edit_{num}'
            # edit = st.button('Edit Plot', key=edit_key, on_click=(lambda key: lambda: set_clicked_button_key(key))(edit_key))

        # Plot button
        if plot:
            #Load Data
            data, data_int=load_data(i[0], i[1], i[2], i[3], i[4], i[5], i[6])

            #Create table for variables presentation in the plot
            tab_data=i[:5]+i[6:]
            index = ['EC:', 'Ph. Index:', 'NH Torus:', 'Cov. :', 'NH Cone:', 'INT:']
            tab=pd.DataFrame({'Variables': index,'Values': tab_data})

            # Call the create_plot() function to get the figure
            fig=(create_plot(data, data_int, tab))
            st.session_state['figure_list'][num]=fig


        if spec_plot:
            #Load Data
            data, data_int=load_data(st.session_state['plot_list'][int(spec_plot_key[10:])][0], 
            st.session_state['plot_list'][int(spec_plot_key[10:])][1],
            st.session_state['plot_list'][int(spec_plot_key[10:])][2],
            st.session_state['plot_list'][int(spec_plot_key[10:])][3],
            st.session_state['plot_list'][int(spec_plot_key[10:])][4],
            st.session_state['plot_list'][int(spec_plot_key[10:])][5],
            st.session_state['plot_list'][int(spec_plot_key[10:])][6])

            #Create table for variables presentation in the plot
            tab_data=st.session_state['plot_list'][int(spec_plot_key[10:])][:5]+st.session_state['plot_list'][int(spec_plot_key[10:])][6:]
            index = ['EC:', 'Ph. Index:', 'NH Torus:', 'Cov. :', 'NH Cone:', 'INT:']
            tab=pd.DataFrame({'Variables': index,'Values': tab_data})

            # Call the create_plot() function to get the figure
            fig=(create_plot(data, data_int,tab))
            st.session_state['figure_list'][num]=fig

        if delete:
            st.write
            st.session_state['plot_list'].pop(int(delete_key[7:]))
            st.session_state['figure_list'].pop(int(delete_key[7:]))
            st.session_state['aio_fig'] = None
            num -= 1
            st.experimental_rerun()


        if st.session_state['figure_list'][num] is not None:
            st.pyplot(st.session_state['figure_list'][num])

        # Save button
        if len(st.session_state['figure_list']) > 0 and save:
            try:
                save_figure(st.session_state['figure_list'][int(save_key[5:])], 
                            st.session_state['plot_list'][int(save_key[5:])][0],
                            st.session_state['plot_list'][int(save_key[5:])][1],
                            st.session_state['plot_list'][int(save_key[5:])][2],
                            st.session_state['plot_list'][int(save_key[5:])][3],
                            st.session_state['plot_list'][int(save_key[5:])][4])
                st.success('Figure saved locally as output.jpg')
            except Exception as e:
                st.error(f'Error trying to save figure: {e}')
        
        #Save All Button
        if len(st.session_state['figure_list']) > 0 and save_all:
            try:
                j=0
                for i in st.session_state['figure_list']:
                    save_figure(i,
                                st.session_state['plot_list'][j][0],
                                st.session_state['plot_list'][j][1],
                                st.session_state['plot_list'][j][2],
                                st.session_state['plot_list'][j][3],
                                st.session_state['plot_list'][j][4])
                    j+=1
                
                st.success('Figure saved locally as output.jpg')
            except Exception as e:
                st.error(f'Error trying to save figure: {e}')
        
        num +=1

    # Plot all in one button
    if aio_plot and len(st.session_state['plot_list']) > 0:
        st.session_state['aio_fig'] = None

        #Create table for variables presentation in the plot
        tab_data = []
        new_list = [copy.deepcopy(d) for d in st.session_state['plot_list']]
        index = ['EC:', 'Ph. Index:', 'NH Torus:', 'Cov. :', 'NH Cone:', 'INT:']
        for i in new_list:
            i.pop(5)
            tab_data.append(i)
        tab = pd.concat([pd.Series(d) for d in tab_data], axis=1)
        tab.columns = [f'Values {i+1}' for i in range(tab.shape[1])]
        tab.insert(0, 'Variables', index)

        aio_fig=plot_all(st.session_state['plot_list'], tab)
        st.session_state['aio_fig'] = aio_fig
        
    elif aio_plot and len(st.session_state['plot_list']) == 0:
        st.error('No plots created yet')


    if st.session_state['aio_fig'] is not None:
        st.header('All in one Plot:')
        st.pyplot(st.session_state['aio_fig'])
        if st.button('Save all in one Figure'):
            save_aio(st.session_state['aio_fig'])

        

if __name__ == "__main__":
    main()