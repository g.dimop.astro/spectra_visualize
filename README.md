# Welcome to Visualize project

## Basic info

This file will be deleted later.
This is `streamlit` project to visualize the spectra
from our simulations.

1. Add the `par_files` directory with all the output files.
2. Make a simple python file that makes a plot of the first spectrum.
3. The `app.py` is the main streamlit app.
    - It has a button named _plot_.
    - You press it and it shows the figure.

### Basic git commands

- `git checkout XXXX` to change branch
- `git checkout -b YYYY` create new branch and switch to it.

Steps:

- `git status` !super important

1. `git add .` to add the changes you made (stage them).
2. `git commit -m "tralalal"` commit changes.
3. `git push origin YYYYY` push/upload changes to GITLAB.

When you want to sync with remote branch:

1. `git fetch` to sync the metadata
2. `git pull origin ZZZZ` to pull/download/update remote branch to your local branch
3. `git checkout ZZZZ` to go to this branch

EXTRA:
`git checkout -- .` to reset changes **before add**

## New features

### Angles

I made a new branch that plots all the possible observing angles when a set of parameters is selected.
It's a very bad implementation but for now it works.

## Dropdown

I made a new branch to work on the creation of a dropdown list where the user will choose 40,140, 240.
