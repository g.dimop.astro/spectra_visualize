import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import matplotlib.colors as colors
from load import load_data
import os

def create_plot(data, data_int, tab) -> Figure:
    cmap = plt.get_cmap('BuGn')
    
    # Get the minimum and maximum values from your data
    min_value = min(float(key[-2:]) for key in data.keys())
    max_value = max(float(key[-2:]) for key in data.keys())

    # Apply a logarithmic scale to the normalization range
    norm = colors.LogNorm(min_value, max_value)

    fig, ax = plt.subplots(figsize=(10,8))
    for key, value in data.items():
        ax.plot(value[:,0]/1000, value[:,2], lw=3, color=cmap(norm(int(key[-2:]))+0.2), label=key[-2:])
    if len(data_int)>0:
        for key, value in data_int.items():
            ax.plot(value[:,0]/1000, value[:,2], linestyle='--', lw=3, color=cmap(norm(int(key[-2:]))+0.2), label=f"{key[-2:]} INT")
    

    
    #Legend setting
    legend = ax.legend(frameon=True, ncols=2, fontsize=14, title='Different angles', bbox_to_anchor=[1,1], loc='upper left')
    legend.get_frame().set_facecolor('white')
    legend.get_frame().set_alpha(0.7)
    legend.get_title().set_fontsize(17)

    #Table settings
    table = plt.table(cellText=tab.values,
                  colLabels=tab.columns,
                  cellLoc='center',bbox=[1.05, 0, 0.3, 0.3])

    # Hide table borders
    for key, cell in table.get_celld().items():
        cell.set_linewidth(0)
    table.set_fontsize(17)


    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_xlabel('Energy (keV)', fontsize=18)
    ax.set_ylabel('Arbitrary flux', fontsize=18)
    ax.tick_params(axis='both', which='both',top=True, width=1, length=7.5, right=True, labelsize=15, direction='in')

#    fig.tight_layout()

    return plt.gcf()

def save_figure(fig: Figure,EC = 1, PhIndex = 1, NHTor = 1, CF = 1, NHcone = 1) -> None:
    file_name = f'EC_{EC}_G_{PhIndex}_NHtor_{NHTor}_CF_{CF}_NHco_{NHcone}.jpg'
    if os.path.exists(file_name):
        file_name=f'EC_{EC}_G_{PhIndex}_NHtor_{NHTor}_CF_{CF}_NHco_{NHcone}_1.jpg'
        counter = 2
        while os.path.exists(file_name):
            file_name = f'EC_{EC}_G_{PhIndex}_NHtor_{NHTor}_CF_{CF}_NHco_{NHcone}_{counter}.jpg'
            counter += 1

    fig.savefig(file_name, dpi=100)

def save_aio(fig: Figure) -> None:
    file_name = 'All_in_one_Figure.jpg'
    if os.path.exists(file_name):
        file_name='All_in_one_Figure_1.jpg'
        counter = 2
        while os.path.exists(file_name):
            file_name = f'All_in_one_Figure_{counter}.jpg'
            counter += 1
    fig.savefig(file_name, dpi=100)


def plot_all(aio_data, tab) -> Figure:
    colours=['BuGn', 'Oranges', 'Blues', 'Greys', 'Purples', 'Reds', 'RdPu']
    col=0
    col_num=0
    fig, ax = plt.subplots(figsize=(10,8))
    for i in aio_data:
        data, data_int = load_data(i[0], i[1], i[2], i[3], i[4], i[5], i[6])
        cmap = plt.get_cmap(colours[col])
        if col < len(colours)-1:
            col += 1
        else:
            col = 0
    
        # Get the minimum and maximum values from your data
        min_value = min(float(key[-2:]) for key in data.keys())
        max_value = max(float(key[-2:]) for key in data.keys())

        # Apply a logarithmic scale to the normalization range
        norm = colors.LogNorm(min_value, max_value)

        
        for key, value in data.items():
            ax.plot(value[:,0]/1000, value[:,2], lw=3, color=cmap(norm(int(key[-2:]))+0.2), label=key[-2:])
        # col_num += 1
        # legend = ax.legend(frameon=False, ncols=col_num, fontsize=14, title='Different angles')
        if len(data_int)>0:
            for key, value in data_int.items():
                ax.plot(value[:,0]/1000, value[:,2], linestyle='--', lw=3, color=cmap(norm(int(key[-2:]))+0.2), label=f"{key[-2:]} INT")
        col_num += 1
    
    legend = ax.legend(frameon=True, ncols=col_num, fontsize=14, title='Different angles', bbox_to_anchor=[1,1], loc='upper left')
    legend.get_frame().set_facecolor('white')
    legend.get_frame().set_alpha(0.7)
    legend.get_title().set_fontsize(17)

    #Table settings
    table = plt.table(cellText=tab.values,
                  colLabels=tab.columns,
                  cellLoc='center', loc= 'lower left', bbox=[1,0, 0.4,0.4 ])
    

    # Set the desired color
    col=0
    for column_name in tab.columns:
        if column_name.startswith('Values'):
            cmap = plt.get_cmap(colours[col])
            if col < len(colours)-1:
                col += 1
            else:
                col = 0
            column_index = tab.columns.get_loc(column_name)
            for i, cell in enumerate(table.get_celld().values()):
                if i % len(tab.columns) == column_index:
                    cell.set_text_props(color=cmap(cmap.N - 1))  

    # Highlight rows with differences with a yellow background color
    a=len(tab.columns)
    tab=tab.drop('Variables', axis=1)
    dif_list = tab[tab.apply(lambda row: any(row != row.iloc[0]), axis=1)].index
    for col in range(0,a):
        for i in dif_list:
            cell = table.get_celld()[(i+1, col)]
            cell.set_facecolor((1, 1, 0.5))




    # Hide table borders
    for key, cell in table.get_celld().items():
        cell.set_linewidth(0)
    table.set_fontsize(17)


    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_xlabel('Energy (keV)', fontsize=18)
    ax.set_ylabel('Arbitrary flux', fontsize=18)
    ax.tick_params(axis='both', which='both',top=True, width=1, length=7.5, right=True, labelsize=15, direction='in')

    return plt.gcf()