import streamlit as st



def params():   

    # Set the available degrees
    available_degrees = [ 5, 15, 25, 35, 45, 55, 65, 75, 85 ]
    degrees = st.sidebar.multiselect("Select angles or leave empty to select all:", available_degrees)
    degrees= sorted(degrees)

    #Parameter selection
    options_EC = ['40', '140', '240']
    selected_option_EC = st.sidebar.selectbox('Select EC',  options_EC)

    options_PhIndex = ['1.6', '2.0', '2.4']
    selected_option_PhIndex = st.sidebar.selectbox('Select Photon Index:', options_PhIndex)

    options_NHTor = ['23.0', '24.0', '25.0']
    selected_option_NHTor = st.sidebar.selectbox('Select NH Torus:', options_NHTor)

    options_CF = ['0.1', '0.5', '0.9']
    selected_option_CF = st.sidebar.selectbox('Covering Factor:', options_CF)

    options_NHcone = ['22.0', '23.0', '24.0']
    selected_option_NHcone = st.sidebar.selectbox('Select NH Cone:', options_NHcone)
    if len(degrees)==0:
        degrees=available_degrees

    # Create a checkbox to include interaction data
    st.sidebar.write('Check box to add INT component:')
    checkbox_value = st.sidebar.checkbox("INT")

    return selected_option_EC, selected_option_PhIndex, selected_option_NHTor, selected_option_CF, selected_option_NHcone, degrees, checkbox_value